plugins {
    kotlin("jvm") version "2.0.0"
}

group = "center.sciprog"
version = "1.0"

// Where to get dependencies
repositories {
    mavenCentral()
}

dependencies {
    //loading coroutines
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.1")

    //testing libraries
    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.10.3")
}

// Load testing platform
tasks.test {
    useJUnitPlatform()
}

kotlin{
    jvmToolchain(11)
}